import React from 'react';
import { render } from 'react-dom';
import { Signup } from './components/Signup';

class App extends React.Component {
	render() {
		return(
			<Signup/>
		);
	}
}

render(<App/>, window.document.getElementById('app'));